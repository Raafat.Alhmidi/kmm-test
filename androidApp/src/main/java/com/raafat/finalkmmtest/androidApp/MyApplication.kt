package com.raafat.finalkmmtest.androidApp

import android.app.Application
import android.content.Intent
import com.raafat.finalkmmtest.androidApp.ui.ErrorActivity
import java.io.PrintWriter
import java.io.StringWriter

/**
 * Created by Raafat Alhmidi on 15/2/2021 @12:19 PM.
 */
class MyApplication:Application() {

    fun throwableToString(t: Throwable): String? {
        val sw = StringWriter()
        val p = PrintWriter(sw)
        t.printStackTrace(p)
        val s = sw.toString()
        p.close()
        return s
    }
    override fun onCreate() {
        super.onCreate()
        Thread.setDefaultUncaughtExceptionHandler { t: Thread?, e: Throwable ->
            e.printStackTrace()
            val intent = Intent(this, ErrorActivity::class.java)
            intent.putExtra(ErrorActivity.ERROR_KEY, throwableToString(e))
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(intent)
            System.exit(2)
        }
    }
}