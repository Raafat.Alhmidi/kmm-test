package com.raafat.finalkmmtest.androidApp.ui;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.os.Bundle;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.raafat.finalkmmtest.androidApp.R;
import com.raafat.finalkmmtest.androidApp.databinding.ActivityErrorBinding;


public class ErrorActivity extends AppCompatActivity {
    public static final String ERROR_KEY = "ERROR_KEY";
    ActivityErrorBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_error);
        String error = getIntent().getExtras().getString(ERROR_KEY);
        ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
        ClipData clip = ClipData.newPlainText("Exception", error);
        clipboard.setPrimaryClip(clip);
        Toast.makeText(this,"Error Copied to clipboard", Toast.LENGTH_LONG).show();
        binding.errorTV.setText(error);
    }
}
