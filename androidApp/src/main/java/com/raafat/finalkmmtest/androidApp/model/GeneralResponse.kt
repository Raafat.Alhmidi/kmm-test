package com.raafat.finalkmmtest.androidApp.model

/**
 * Created by Raafat Alhmidi on 12/2/2021 @11:12 PM.
 */
data class GeneralResponse<T>(
    val data:T?,
    val status:Status,
    val errorMsg:String?= null
){

    companion object{
        fun <T> success(data:T):GeneralResponse<T> = GeneralResponse(data,Status.SUCCSESS)

        fun <T> error(errorMsg: String):GeneralResponse<T> = GeneralResponse(null,Status.ERROR,errorMsg)

        fun <T> loading():GeneralResponse<T> = GeneralResponse(null,Status.LOADING)
    }
}

enum class Status{
    LOADING,
    SUCCSESS,
    ERROR
}