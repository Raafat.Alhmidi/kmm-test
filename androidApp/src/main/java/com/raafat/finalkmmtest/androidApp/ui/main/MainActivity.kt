package com.raafat.finalkmmtest.androidApp.ui.main

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.google.android.material.tabs.TabLayout
import com.raafat.finalkmmtest.androidApp.R
import com.raafat.finalkmmtest.androidApp.databinding.ActivityMainBinding
import com.raafat.finalkmmtest.androidApp.model.Status
import com.raafat.finalkmmtest.shared.model.User
import com.raafat.finalkmmtest.shared.utils.InputValidation
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.cancel
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.take
import kotlinx.coroutines.launch

class MainActivity : AppCompatActivity(), View.OnClickListener {

    private lateinit var binding: ActivityMainBinding
    private var currentUser: User? = null
    private lateinit var viewModel: MainViewModel
    private val mainScope = MainScope()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        viewModel = ViewModelProvider.AndroidViewModelFactory(application).create(MainViewModel::class.java)
        //checkIfLoggedin()
        binding.loginRegistraionTab.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab?) {
                if (tab?.position == 1) {
                    binding.motionLayout.transitionToState(R.id.login)
                } else if (tab?.position == 0) {
                    binding.motionLayout.transitionToState(R.id.register)
                }
                binding.root.clearFocus()
                binding.firstNameTIL.isErrorEnabled = false
                binding.lastNameTIL.isErrorEnabled = false
                binding.userNameTIL.isErrorEnabled = false
                binding.passwordTIL.isErrorEnabled = false
                binding.emailTIL.isErrorEnabled = false
            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {
            }

            override fun onTabReselected(tab: TabLayout.Tab?) {
            }
        })

    }

    override fun onResume() {
        super.onResume()
    }
    override fun onDestroy() {
        super.onDestroy()
        mainScope.cancel()
    }


    private fun validateRegister(user: User): Boolean = when {

        user.firstName.isBlank() -> {
            binding.firstNameTIL.isErrorEnabled = true
            binding.firstNameTIL.error = "Please enter first name"
            binding.firstNameTIL.requestFocusFromTouch()
            false
        }

        user.lastName.isEmpty() -> {
            binding.lastNameTIL.isErrorEnabled = true
            binding.lastNameTIL.error = "Please enter last name"
            binding.lastNameTIL.requestFocus()
            false
        }

        user.userName.isEmpty() -> {
            binding.userNameTIL.isErrorEnabled = true
            binding.userNameTIL.error = "Please enter username"
            binding.userNameTIL.requestFocus()
            false
        }

        user.password.isEmpty() -> {
            binding.passwordTIL.isErrorEnabled = true
            binding.passwordTIL.error = "Please enter password"
            binding.passwordTIL.requestFocus()
            false
        }

        user.email.isBlank() -> {
            binding.emailTIL.isErrorEnabled = true
            binding.emailTIL.error = "Please enter email address"
            binding.emailTIL.requestFocusFromTouch()
            false
        }

        !InputValidation.isValidEmail(user.email) -> {
            binding.emailTIL.isErrorEnabled = true
            binding.emailTIL.error = "Not a valid email address"
            binding.emailTIL.requestFocusFromTouch()
            false
        }
        else -> true
    }

    private fun validateLogin(): Boolean = when {
        binding.userNameTIET.text.isNullOrEmpty() -> {
            binding.userNameTIL.isErrorEnabled = true
            binding.userNameTIL.error = "Please enter user name"
            binding.userNameTIL.requestFocusFromTouch()
            false
        }

        binding.passwordTIET.text.isNullOrEmpty() -> {
            binding.passwordTIL.isErrorEnabled = true
            binding.passwordTIL.error = "Please enter password"
            binding.passwordTIL.requestFocus()
            false
        }

//        !usersRepository.isPasswordCorrect(binding.userNameTIET.text.toString(), binding.passwordTIET.text.toString()) -> {
//            binding.passwordTIL.isErrorEnabled = true
//            binding.passwordTIL.error = "Incorrect password"
//            binding.passwordTIL.requestFocus()
//            false
//        }
        else -> true
    }


    private fun handleLoginClick() {
        binding.firstNameTIL.isErrorEnabled = false
        binding.lastNameTIL.isErrorEnabled = false
        binding.userNameTIL.isErrorEnabled = false
        binding.passwordTIL.isErrorEnabled = false
        binding.emailTIL.isErrorEnabled = false
        if (validateLogin()) {
            mainScope.launch {
                viewModel.getUserInfo(binding.userNameTIET.text.toString(), binding.passwordTIET.text.toString()).collect {
                    when (it.status) {
                        Status.LOADING -> binding.motionLayout.transitionToState(R.id.loading)
                        Status.ERROR -> {
                            binding.motionLayout.transitionToState(R.id.login_final_stage)
                            Toast.makeText(this@MainActivity, it.errorMsg, Toast.LENGTH_SHORT).show()
                        }
                        Status.SUCCSESS -> {
                            currentUser = it.data
                            binding.user = currentUser
                            binding.motionLayout.transitionToState(R.id.profile)

                        }
                    }
                }
            }
        }
    }

    private fun checkIfLoggedin(){
        mainScope.launch {
            viewModel.getLoggedinUser().collect {
                when (it.status) {
                    Status.LOADING -> binding.motionLayout.transitionToState(R.id.loading)
                    Status.SUCCSESS -> {
                        currentUser = it.data
                        if (currentUser == null) {
                            binding.motionLayout.transitionToState(R.id.register)
                        } else {
                            binding.motionLayout.transitionToState(R.id.profile)
                            binding.user = currentUser
                        }
                    }
                }
            }
        }
    }

    private fun handleEditClick() {
        binding.motionLayout.transitionToState(R.id.edit)
    }

    private fun handleSaveClick() {
        val newUser = currentUser?.copy()
        newUser?.let{
            with(it){
                firstName = binding.firstNameTIET.text.toString()
                lastName = binding.lastNameTIET.text.toString()
                password = binding.passwordTIET.text.toString()
                email = binding.emailTIET.text.toString()
            }
            if(validateRegister(it)){
                mainScope.launch {
                    viewModel.updateUser(it).collect { value ->
                        when (value.status) {
                            Status.LOADING -> binding.motionLayout.transitionToState(R.id.loading)
                            Status.ERROR -> {
                                binding.motionLayout.transitionToState(R.id.register)
                                Toast.makeText(this@MainActivity, value.errorMsg, Toast.LENGTH_SHORT).show()
                            }
                            Status.SUCCSESS -> {
                                currentUser = newUser
                                binding.user = currentUser
                                binding.motionLayout.transitionToState(R.id.profile)
                            }
                        }
                    }

                }
            }
        }
    }

    private fun handleRegisterClick() {
        binding.firstNameTIL.isErrorEnabled = false
        binding.lastNameTIL.isErrorEnabled = false
        binding.userNameTIL.isErrorEnabled = false
        binding.passwordTIL.isErrorEnabled = false
        binding.emailTIL.isErrorEnabled = false

        val newUser = User(
            binding.firstNameTIET.text.toString().trim(),
            binding.lastNameTIET.text.toString().trim(),
            binding.userNameTIET.text.toString().trim(),
            binding.passwordTIET.text.toString().trim(),
            binding.emailTIET.text.toString().trim(),
            null
        )
        if (validateRegister(newUser))
            mainScope.launch {
                viewModel.registerUser(newUser).take(2).collect { value ->
                    when (value.status) {
                        Status.LOADING -> binding.motionLayout.transitionToState(R.id.loading)
                        Status.ERROR -> {
                            binding.motionLayout.transitionToState(R.id.register)
                            Toast.makeText(this@MainActivity, value.errorMsg, Toast.LENGTH_SHORT).show()
                        }
                        Status.SUCCSESS -> {
                            currentUser = newUser
                            binding.user = currentUser
                            binding.motionLayout.transitionToState(R.id.profile)
                        }
                    }
                }
            }

    }

    private fun handleLogoutClick() {
        mainScope.launch {
            currentUser?.let {
                viewModel.logoutUser(it).collect {
                    when (it.status) {
                        Status.LOADING -> binding.motionLayout.transitionToState(R.id.loading)
                        Status.ERROR -> {
                            binding.motionLayout.transitionToState(R.id.profile)
                            Toast.makeText(this@MainActivity, it.errorMsg, Toast.LENGTH_SHORT).show()
                        }
                        Status.SUCCSESS->{
                            clearViews()
                            binding.user = null
                            binding.motionLayout.transitionToState(R.id.register)
                        }
                    }
                }
            }
        }
    }

    private fun clearViews() {
        with(binding) {
            emailTIET.setText("")
            userNameTIET.setText("")
            passwordTIET.setText("")
            firstNameTIET.setText("")
            lastNameTIET.setText("")
            loginRegistraionTab.getTabAt(0)?.select()
        }
    }

    override fun onClick(v: View?) {
        val id = v?.id
        when (id) {
            R.id.logoutBtn -> {
                handleLogoutClick()

            }
            R.id.historyBtn -> {

            }
            R.id.registerBtn -> {
                when (binding.motionLayout.currentState) {
                    R.id.login_final_stage -> handleLoginClick()
                    R.id.profile -> handleEditClick()
                    R.id.register -> handleRegisterClick()
                    R.id.edit -> handleSaveClick()
                }
            }
            R.id.startBtn-> checkIfLoggedin()

        }
    }
}
