package com.raafat.finalkmmtest.androidApp.ui.main

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.ViewModel
import com.raafat.finalkmmtest.androidApp.model.GeneralResponse
import com.raafat.finalkmmtest.shared.cache.DatabaseDriverFactory
import com.raafat.finalkmmtest.shared.model.User
import com.raafat.finalkmmtest.shared.repositories.UsersRepository
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.flow

/**
 * Created by Raafat Alhmidi on 12/2/2021 @11:11 PM.
 */
class MainViewModel(application: Application) : AndroidViewModel(application) {

    private val usersRepository = UsersRepository(DatabaseDriverFactory(getApplication()))

    init {

    }

    fun registerUser(user: User) = flow {
        emit(GeneralResponse.loading<Nothing>())
        delay(1500)
        if (!usersRepository.isEmailAvailable(user.email))
            emit(GeneralResponse.error<Any>("Email is already exists!"))
        else if (!usersRepository.isUsernameAvailable(user.userName))
            emit(GeneralResponse.error<Any>("Username is already exists!"))
        else {
            kotlin.runCatching {
                usersRepository.registerUser(user, System.currentTimeMillis())
            }.onSuccess {
                emit(GeneralResponse.success<Any?>(null))
            }.onFailure {
                emit(GeneralResponse.error<Any>(it.message ?: "General Error:("))
            }
        }
    }

    fun getLoggedinUser() = flow {
        emit(GeneralResponse.loading())
        delay(1500)
        emit(GeneralResponse.success(usersRepository.getLogedinUser()))
    }

    fun getUserInfo(username: String, password: String) = flow {
        emit(GeneralResponse.loading<User?>())
        delay(1500)
        val user = usersRepository.getUserInfo(username, password)
        if (user == null) {
            emit(GeneralResponse.error<User?>("Username or password incorrect"))
        } else {
            emit(GeneralResponse.success(user))
            usersRepository.loginUser(user,System.currentTimeMillis())
        }
    }

    fun logoutUser(user: User) = flow<GeneralResponse<Any?>> {
        emit(GeneralResponse.loading())
        delay(1000)
        usersRepository.logoutUser(user, System.currentTimeMillis())
        emit(GeneralResponse.success(null))
    }

    fun updateUser(user: User) = flow<GeneralResponse<Any?>> {
        emit(GeneralResponse.loading())
        delay(1500)
        if (!usersRepository.isEmailAvailable(user.email,user.userName))
            emit(GeneralResponse.error("Email is already exists!"))
        else {
            usersRepository.updateUser(user)
            emit(GeneralResponse.success(null))
        }
    }
}