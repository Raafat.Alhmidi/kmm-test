plugins {
    id("com.android.application")
    kotlin("android")
}

val progressHudVersoin = "V1.0.3"
dependencies {
    implementation(project(":shared"))
    implementation("com.google.android.material:material:1.2.1")
    implementation("androidx.appcompat:appcompat:1.2.0")
    implementation("androidx.core:core-ktx:1.3.2")

    api("org.jetbrains.kotlinx:kotlinx-coroutines-android:1.3.9")
    api("org.jetbrains.kotlinx:kotlinx-coroutines-core:1.3.9")
    implementation("androidx.constraintlayout:constraintlayout:2.0.2")
    implementation("com.github.RHoumaidy:ProgressHUD:$progressHudVersoin")
}

android {
    signingConfigs {

        create("release") {
            keyAlias = "Key0"
            keyPassword = "azsxdc"
            storeFile = file("./KeyStore.jks")
            storePassword = "azsxdc"
        }
    }
    compileSdkVersion(29)
    defaultConfig {
        applicationId = "com.raafat.finalkmmtest.androidApp"
        minSdkVersion(24)
        targetSdkVersion(29)
        versionCode = 1
        versionName = "1.0"
    }
    buildTypes {
        getByName("release") {
            isMinifyEnabled = false
            signingConfig = signingConfigs.getByName("release")
        }
    }

    buildFeatures {
        dataBinding = true
    }
    compileOptions {
        targetCompatibility = JavaVersion.VERSION_1_8
    }
}