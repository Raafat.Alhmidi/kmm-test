package com.raafat.finalkmmtest.shared.utils
import android.util.Patterns

/**
 * Created by Raafat Alhmidi on 11/2/2021 @2:10 PM.
 */
actual object InputValidation {
    actual fun isValidEmail(email:String):Boolean{
        return email.isNotEmpty() && Patterns.EMAIL_ADDRESS.matcher(email).matches()
    }
}