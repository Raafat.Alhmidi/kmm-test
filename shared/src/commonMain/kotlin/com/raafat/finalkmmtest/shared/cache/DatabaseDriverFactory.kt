package com.raafat.finalkmmtest.shared.cache

import com.squareup.sqldelight.db.SqlDriver

/**
 * Created by Raafat Alhmidi on 11/2/2021 @3:43 AM.
 */
expect class DatabaseDriverFactory {
    fun createDriver(): SqlDriver
}