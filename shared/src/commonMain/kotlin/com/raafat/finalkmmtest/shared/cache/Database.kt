package com.raafat.finalkmmtest.shared.cache

import com.raafat.finalkmmtest.shared.model.User
import com.raafat.finalkmmtest.shared.model.UserSession
import kotlin.math.log

internal class Database(databaseDriverFactory: DatabaseDriverFactory) {
    private val database = AppDatabase(databaseDriverFactory.createDriver())
    private val dbQuery = database.appDatabaseQueries

    internal fun clearDatabase() {
        dbQuery.transaction {
            dbQuery.removeAllSessions()
            dbQuery.removeAllUsers()
        }
    }

    internal fun registerUser(user: User, time: Long) {
        dbQuery.transaction {
            insertUser(user)
            loginUser(user, time)
        }
    }

    internal fun loginUser(user: User, time: Long) {
            insertSession(UserSession(loginTime = time, userName = user.userName))

    }

    internal fun logoutUser(user: User, time: Long) {
        dbQuery.logoutUser(time, user.userName)
    }

    internal fun getLoggedinUser(): User? {
        return dbQuery.getLoggedinUser(::mapUserInfo).executeAsOneOrNull()
    }

    internal fun getUserInfo(userName: String, password: String): User? {
        val user = dbQuery.getUserInfo(userName, password, ::mapUserInfo).executeAsList()
        val sessions = user.flatMap { it.sessions ?: listOf() }
        return user.firstOrNull()?.apply {
            this.sessions = sessions
        }
    }

    private fun mapUserInfo(
        firstName: String,
        lastName: String,
        userName: String,
        password: String,
        email: String,
        id: Int,
        loginTime: Long,
        logoutTime: Long?,
        userName_: String
    ): User = User(
        firstName = firstName,
        lastName = lastName,
        userName = userName,
        password = password,
        email = email,
        sessions = listOf(
            UserSession(
                id = id,
                loginTime = loginTime,
                logoutTime = logoutTime,
                userName = userName_
            )
        )
    )

    private fun insertSession(session: UserSession) {
        dbQuery.loginUser(
            loginTime = session.loginTime,
            userName = session.userName
        )
    }

    private fun insertUser(user: User) {
        dbQuery.registerUser(
            firstName = user.firstName,
            lastName = user.lastName,
            email = user.email,
            password = user.password,
            userName = user.userName,
        )
    }


    internal fun isEmailExists(email: String): Boolean = dbQuery.isEmailExists(email).executeAsOne()

    internal fun isEmailExists(email: String, userName: String): Boolean =
        dbQuery.isEmailExistsWithUsername(email, userName).executeAsOne()


    internal fun isUsernameExists(userName: String): Boolean = dbQuery.isUsernameExists(userName).executeAsOne()


    internal fun isPasswordCorrect(userName: String, password: String): Boolean =
        dbQuery.isPasswordCorrect(userName, password).executeAsOne()

    internal fun updateUser(user: User) {
        dbQuery.updateUser(user.firstName, user.lastName, user.email, user.password, user.userName)
    }

}