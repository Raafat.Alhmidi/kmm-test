package com.raafat.finalkmmtest.shared.utils

/**
 * Created by Raafat Alhmidi on 11/2/2021 @2:06 PM.
 */
expect object InputValidation{
    fun isValidEmail(email:String):Boolean
}