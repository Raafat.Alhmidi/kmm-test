package com.raafat.finalkmmtest.shared.repositories

import com.raafat.finalkmmtest.shared.cache.Database
import com.raafat.finalkmmtest.shared.cache.DatabaseDriverFactory
import com.raafat.finalkmmtest.shared.model.User

/**
 * Created by Raafat Alhmidi on 11/2/2021 @4:11 AM.
 */
class UsersRepository(databaseDriverFactory: DatabaseDriverFactory) {
    private val database = Database(databaseDriverFactory)

    fun getUserInfo(userName: String,password: String): User? {
        return database.getUserInfo(userName,password)
    }

    fun loginUser(user: User, time: Long) {
        database.loginUser(user, time)
    }

    fun logoutUser(user:User,time:Long){
        database.logoutUser(user,time)
    }

    @Throws(Exception::class) fun registerUser(user: User, time: Long) {
        database.registerUser(user, time)
    }

    fun getLogedinUser():User?{
        return database.getLoggedinUser()
    }

    fun isUsernameAvailable(userName: String):Boolean = !database.isUsernameExists(userName)


    fun isEmailAvailable(email:String):Boolean =  !database.isEmailExists(email)
    fun isEmailAvailable(email:String,userName: String):Boolean =  !database.isEmailExists(email,userName)

    fun isPasswordCorrect(userName: String,password:String):Boolean = database.isPasswordCorrect(userName,password)

    @Throws(Exception::class) fun updateUser(user: User) = database.updateUser(user)

}