package com.raafat.finalkmmtest.shared.model

/**
 * Created by Raafat Alhmidi on 11/2/2021 @3:39 AM.
 */
data class User(
    var firstName:String,
    var lastName:String,
    var userName:String,
    var password:String,
    var email:String,
    var sessions:List<UserSession>?
){
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other == null || this::class != other::class) return false

        other as User

        if (firstName != other.firstName) return false
        if (lastName != other.lastName) return false
        if (userName != other.userName) return false
        if (password != other.password) return false
        if (email != other.email) return false

        return true
    }

    override fun hashCode(): Int {
        var result = firstName.hashCode()
        result = 31 * result + lastName.hashCode()
        result = 31 * result + userName.hashCode()
        result = 31 * result + password.hashCode()
        result = 31 * result + email.hashCode()
        return result
    }
}

data class UserSession(
    val id:Int? = null,
    val loginTime:Long ,
    val logoutTime:Long? = null,
    val userName:String
)