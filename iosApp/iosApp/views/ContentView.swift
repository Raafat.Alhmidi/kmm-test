//
//  ContentView.swift
//  My First Name
//
//  Created by Raafat Alhmidi on 2/14/21.
//

import SwiftUI
import shared


struct Btn:View{
    var lable:String
    var icon:String = "chevron.right"
    
    var onClick:()->() = {}
    var body: some View{
        
        HStack(alignment: .center, spacing: 20){
            Text(lable)
                .foregroundColor(.black)
            Image(systemName: icon)
                .background(Color(red: 0.0, green: 0.0, blue: 0.0, opacity: 0.0))
                
      
        }.frame(width: UIScreen.main.bounds.width-120,height: 40)
        .background(Color("accent"))
        .clipShape(Capsule())
        .padding()
        .onTapGesture(count: /*@START_MENU_TOKEN@*/1/*@END_MENU_TOKEN@*/, perform: {
            onClick()
        })
        

        
                    
    }
}

struct ContentView: View {
    @ObservedObject private(set) var viewModel: ViewModel

    @State var showWelcomeScreen: Bool = true
    @State var showProfile:Bool = false
    var body: some View {
        ZStack{
            Color("backgroundColor")
                .ignoresSafeArea()
            if(showWelcomeScreen){
           
                VStack(spacing: 20, content: {
                    Spacer()
                    Text("Hi").font(/*@START_MENU_TOKEN@*/.title/*@END_MENU_TOKEN@*/).foregroundColor(.white)
                    Text("Hi, This is a demo app for MyMy using KMM (Kotlin Multiplatform Mobile\n Created by Raafat Alhmidi")
                        .multilineTextAlignment(.center)
                        .foregroundColor(.white)
                    Btn(lable: "Start",onClick: {
                        showWelcomeScreen = false
                    })
                    Spacer()
                        
                })
            }else{
                getMainView()
            }
            
        }
        
    }
    
    private func getMainView() -> AnyView{
     
        if(viewModel.sucess){
            
            return AnyView(ProfileView(viewModel:viewModel,onLogout:{
                showProfile = false
            }))
        }else{
            return AnyView(LoginRegisterView(viewModel:self.viewModel,onLoginRegister:{
                showProfile = true
            }))
        }
    }
        
}


struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ProfileView(viewModel: .init( repo: UsersRepository(databaseDriverFactory: DatabaseDriverFactory())), onLogout: {})
    }
}
