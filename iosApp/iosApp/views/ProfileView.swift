//
//  ProfileView.swift
//  iosApp
//
//  Created by Raafat Alhmidi on 2/14/21.
//  Copyright © 2021 orgName. All rights reserved.
//

import Foundation
import SwiftUI
import shared

struct ProfileView:View{
    @ObservedObject private(set) var viewModel: ViewModel

    var onLogout:()->()={}
    
    @State var  isEditing = false
    
    init(viewModel:ViewModel,onLogout:@escaping ()->()) {
        self.onLogout = onLogout
        self.viewModel = viewModel
    }
    
    var body: some View{
        ZStack{
            Color("backgroundColor")
                .ignoresSafeArea()
            VStack(spacing:15){
                HStack{
                    Image(systemName: "arrowshape.turn.up.left")
                        .foregroundColor(.white)
                        .padding(.horizontal,15)
                        .padding(.vertical,15)
                        .fixedSize(horizontal: false, vertical: /*@START_MENU_TOKEN@*/true/*@END_MENU_TOKEN@*/)
                        .onTapGesture(count: /*@START_MENU_TOKEN@*/1/*@END_MENU_TOKEN@*/, perform: {
                            viewModel.logoutUser()
                            onLogout()
                        })
                    
                    Image(systemName: "")
                        .frame(maxWidth:.infinity)

                    Image(systemName: isEditing ? "checkmark" :  "pencil")
                        .foregroundColor(.white)
                        .padding(.horizontal,15)
                        .padding(.vertical,15)
                        .fixedSize(horizontal: false, vertical: /*@START_MENU_TOKEN@*/true/*@END_MENU_TOKEN@*/)
                        .onTapGesture(count: /*@START_MENU_TOKEN@*/1/*@END_MENU_TOKEN@*/, perform: {
                            if(isEditing){
                                viewModel.updateUser()
                                
                            }
                            isEditing = !isEditing
                        })

                    
                    
                }
                Spacer()
                Text(isEditing ? "Edit" : "Profile")
                    .font(.system(size: 60 ,weight: .semibold))
                    .foregroundColor(.white)
                HStack (spacing:20){
                    HStack{
                        Image(systemName: "info.circle")
                        TextField("First Name",text:$viewModel.currentUser.firstName)
                            .foregroundColor(.black)
                            .disabled(!isEditing)
                            .disableAutocorrection(true)
                            .autocapitalization(.words)
                    }.frame(height:50)
                    .padding(.horizontal,20)
                    .background(Color.white)
                    .cornerRadius(8.0)
                    HStack{
                        Image(systemName: "info.circle")
                        TextField("last Name",text:$viewModel.currentUser.lastName)
                            .foregroundColor(.black)
                            .disabled(!isEditing)
                            .disableAutocorrection(true)
                            .autocapitalization(.words)
                    }.frame(height:50)
                    .padding(.horizontal,20)
                    .background(Color.white)
                    .cornerRadius(8.0)
                }.padding(.horizontal,20)
                HStack{
                    Image(systemName: "person")
                    Text(viewModel.currentUser.userName)
                        .foregroundColor(.black)
                        .frame(maxWidth:.infinity,alignment:.leading)
                }.frame(height:50)
                .padding(.horizontal,20)
                .background(Color.white)
                .cornerRadius(8.0)
                .padding(.horizontal,20)
                
                HStack{
                    Image(systemName: "lock")
                    TextField("Password",text:$viewModel.currentUser.password)
                        .foregroundColor(.black)
                        .disabled(!isEditing)
                        .keyboardType(.numberPad)
                        .autocapitalization(/*@START_MENU_TOKEN@*/.none/*@END_MENU_TOKEN@*/)
                        .disableAutocorrection(true)
                }.frame(height:50)
                .padding(.horizontal,20)
                .background(Color.white)
                .cornerRadius(8.0)
                .padding(.horizontal,20)
                
                HStack{
                    Image(systemName: "envelope")
                    TextField("Email",text:$viewModel.currentUser.email)
                        .foregroundColor(.black)
                        .disabled(!isEditing)
                        .keyboardType(.emailAddress)
                        .autocapitalization(/*@START_MENU_TOKEN@*/.none/*@END_MENU_TOKEN@*/)
                    
                }.frame(height:50)
                .padding(.horizontal,20)
                .background(Color.white)
                .cornerRadius(8.0)
                .padding(.horizontal,20)
                Spacer()
            }
        }
    }
}
