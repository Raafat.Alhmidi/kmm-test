//
//  RegisterView.swift
//  iosApp
//
//  Created by Raafat Alhmidi on 2/14/21.
//  Copyright © 2021 orgName. All rights reserved.
//

import SwiftUI
import shared

struct RegisterView:View{
    @ObservedObject private(set) var viewModel: ViewModel
    var onRegister:()->()={}
  
    @State var isError = false
    @State var errorMsg = ""
    var body: some View{
        ZStack{
            Color("backgroundColor")
                .ignoresSafeArea()
            VStack(spacing:15){
                Spacer()
                Text("Register")
                    .font(.system(size: 60 ,weight: .semibold)).foregroundColor(.white)
                HStack (spacing:20){
                    HStack{
                        Image(systemName: "info.circle")
                        TextField("First Name",text:$viewModel.currentUser.firstName)
                            .foregroundColor(.black)
                            .disableAutocorrection(true)
                            .autocapitalization(.words)
                    }.frame(height:50)
                    .padding(.horizontal,20)
                    .background(Color.white)
                    .cornerRadius(8.0)
                    .autocapitalization(.words)
                    HStack{
                        Image(systemName: "info.circle")
                        TextField("last Name",text:$viewModel.currentUser.lastName)
                            .foregroundColor(.black)
                            .disableAutocorrection(true)
                            .autocapitalization(.words)
                    }.frame(height:50)
                    .padding(.horizontal,20)
                    .background(Color.white)
                    .cornerRadius(8.0)
                    .autocapitalization(.words)
                }.padding(.horizontal,20)
                HStack{
                    Image(systemName: "person")
                    TextField("Username",text:$viewModel.currentUser.userName)
                        .foregroundColor(.black)
                        .disableAutocorrection(true)
                        .autocapitalization(.none)
                }.frame(height:50)
                .padding(.horizontal,20)
                .background(Color.white)
                .cornerRadius(8.0)
                .padding(.horizontal,20)
                .autocapitalization(.none)
                
                HStack{
                    Image(systemName: "lock")
                    TextField("Password",text:$viewModel.currentUser.password)
                        .foregroundColor(.black)
                        .autocapitalization(/*@START_MENU_TOKEN@*/.none/*@END_MENU_TOKEN@*/)
                        .disableAutocorrection(true)
                        .keyboardType(.numberPad)
                }.frame(height:50)
                .padding(.horizontal,20)
                .background(Color.white)
                .cornerRadius(8.0)
                .padding(.horizontal,20)
                .autocapitalization(.none)
                
                HStack{
                    Image(systemName: "envelope")
                    TextField("Email",text:$viewModel.currentUser.email)
                        .foregroundColor(.black)
                        .keyboardType(.emailAddress)
                        .disableAutocorrection(true)
                        .autocapitalization(/*@START_MENU_TOKEN@*/.none/*@END_MENU_TOKEN@*/)
                }.frame(height:50)
                .padding(.horizontal,20)
                .background(Color.white)
                .cornerRadius(8.0)
                .padding(.horizontal,20)
                .autocapitalization(.none)
                
                Btn(lable: "Register",onClick: {
                    viewModel.registerUser()
                    if(viewModel.sucess){
                        onRegister()
                    }else{
                        isError = true
                        errorMsg = viewModel.errorMsg
                    }
                })
                Spacer()
                if(isError){
                    Text(errorMsg)
                        .foregroundColor(.red)
                        .padding(.vertical,20)
                        .padding(.horizontal,10)
                }
                    
            }
        }
    }
}





struct RegisterView_Previews: PreviewProvider {
    static var previews: some View {
        RegisterView(viewModel: .init(repo: UsersRepository(databaseDriverFactory: DatabaseDriverFactory())))
    }
}
