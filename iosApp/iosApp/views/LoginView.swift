//
//  LoginView.swift
//  iosApp
//
//  Created by Raafat Alhmidi on 2/14/21.
//  Copyright © 2021 orgName. All rights reserved.
//

import SwiftUI
import shared

struct LoginView:View{
    @ObservedObject private(set) var viewModel: ViewModel
    var onLogin:()->()={}
    @State var userName = ""
    @State var password = ""
    
    @State var isError = false
    @State var errorMsg = ""
    var body: some View{
        ZStack{
            Color("backgroundColor")
                .ignoresSafeArea()
            VStack(spacing:15){
                Spacer()
                Text("Longin")
                    .font(.system(size: 60 ,weight: .semibold)).foregroundColor(.white)
                HStack{
                    Image(systemName: "person")
                    TextField("Username",text:$userName)
                        .foregroundColor(.black)
                        .autocapitalization(.none)
                        .disableAutocorrection(true)
                        
                }.frame(height:50)
                .padding(.horizontal,20)
                .background(Color.white)
                .cornerRadius(8.0)
                .padding(.horizontal,20)
                
                HStack{
                    Image(systemName: "lock")
                    TextField("Password",text:$password)
                        .foregroundColor(.black)
                        .autocapitalization(.none)
                        .keyboardType(.numberPad)
                }.frame(height:50)
                .padding(.horizontal,20)
                .background(Color.white)
                .cornerRadius(8.0)
                .padding(.horizontal,20)
                
                Btn(lable: "Login",onClick: {
                    viewModel.loginUser(userName: userName, password: password)
                    if(viewModel.sucess){
                        onLogin()
                    }else{
                        isError = true
                        errorMsg = viewModel.errorMsg
                    }
                })
                Spacer()
                if(isError){
                    Text(errorMsg)
                        .foregroundColor(.red)
                        .padding(.vertical,20)
                        .padding(.horizontal,10)
                }
                    
            }
        }
    }
}


struct LoginView_Previews: PreviewProvider {
    static var previews: some View {
        LoginView(viewModel: .init(repo: UsersRepository(databaseDriverFactory: DatabaseDriverFactory())))
    }
}
