//
//  LoginRegisterView.swift
//  iosApp
//
//  Created by Raafat Alhmidi on 2/14/21.
//  Copyright © 2021 orgName. All rights reserved.
//

import Foundation
import SwiftUI

struct LoginRegisterView:View{
    @ObservedObject private(set) var viewModel: ViewModel
    let onLoginRegister:()->()
    init(viewModel:ViewModel,onLoginRegister:@escaping ()->()) {
        UITabBar.appearance().isTranslucent = false
        UITabBar.appearance().barTintColor = UIColor(red: 0.07, green: 0.07, blue: 0.07, alpha: 0)
        self.onLoginRegister = onLoginRegister
        self.viewModel = viewModel

    }
    var body: some View{
       
        TabView {
            LoginView(viewModel: viewModel, onLogin:{
                onLoginRegister()
            })
                .tabItem {
                    Label("Login", systemImage: "person.icloud").foregroundColor(.white)
                }

            RegisterView( viewModel: viewModel, onRegister:{
                onLoginRegister()
            })
                .tabItem {
                    Label("Register", systemImage: "square.and.pencil")
                }
        }.accentColor(Color("accent"))
        
    }
}

