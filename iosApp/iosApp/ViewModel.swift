//
//  ViewModel.swift
//  iosApp
//
//  Created by Raafat Alhmidi on 2/14/21.
//  Copyright © 2021 orgName. All rights reserved.
//

import Foundation
import shared


class ViewModel: ObservableObject {
    let repository: UsersRepository
    @Published var sucess:Bool = true
    @Published var errorMsg:String = ""
    @Published var currentUser:User = User(firstName: "", lastName: "", userName: "", password: "", email: "", sessions: nil)
    init(repo: UsersRepository) {
        self.repository = repo
        self.currentUser = getLoggedInUser() ?? User(firstName: "", lastName: "", userName: "", password: "", email: "", sessions: nil)
    }

    func getLoggedInUser()->User? {
        let user = repository.getLogedinUser()
        
        sucess = user != nil
        return user
        
    }
    
    func registerUser() {
        sucess = true
        errorMsg = ""
        if(currentUser.firstName.isEmpty){
            sucess = false
            errorMsg += "* First name conn't be empty"
        }
        if(currentUser.lastName.isEmpty){
            sucess = false
            errorMsg += "\n* Last name conn't be empty"
        }
        if(currentUser.email.isEmpty){
            sucess = false
            errorMsg += "\n* Email conn't be empty"
        }
        if(currentUser.password.isEmpty){
            sucess = false
            errorMsg += "\n* Password cann't be empty"
            
        }
        if(sucess){
            do {
                try repository.registerUser(user: currentUser, time: Int64(Date().timeIntervalSince1970 * 1000))
            } catch let e {
                sucess = false
                errorMsg = "General Error "+e.localizedDescription
            }
            
        }
        
    }
    
    func logoutUser(){
        sucess = false
        repository.logoutUser(user: currentUser, time: Int64(Date().timeIntervalSince1970 * 1000))
        
    }
    
    func loginUser(userName:String,password:String){
        sucess = true
        errorMsg = ""
        if(userName.isEmpty){
            sucess = false
            errorMsg = "* UserName conn't be empty"
        }
        if(password.isEmpty){
            sucess = false
            errorMsg += "\n* Password cann't be empty"
        }
        
        if(sucess){
            let user = repository.getUserInfo(userName: userName, password: password)
            if(user == nil){
                sucess = false
                errorMsg = "Username or Passowr incorrect :("
            }else{
                currentUser = user!
                repository.loginUser(user: user!, time: Int64(Date().timeIntervalSince1970 * 1000))
                sucess  = true
            }
        }
    }
    
    func updateUser(){
        
        sucess = true
        errorMsg = ""
        if(currentUser.firstName.isEmpty){
            sucess = false
            errorMsg += "* First name conn't be empty"
        }
        if(currentUser.lastName.isEmpty){
            sucess = false
            errorMsg += "\n* Last name conn't be empty"
        }
        if(currentUser.email.isEmpty){
            sucess = false
            errorMsg += "\n* Email conn't be empty"
        }
        if(currentUser.password.isEmpty){
            sucess = false
            errorMsg += "\n* Password cann't be empty"
            
        }
        if(sucess){
            do {
                try repository.updateUser(user: currentUser)
            } catch let e {
                sucess = false
                errorMsg = "General Error "+e.localizedDescription
            }
            
        }
    }
}
